import axios from 'axios';
import * as moment from 'moment';
import { getHostUri } from '../../config/api';
import { ACTS_FETCH_FAIL, ACTS_FETCH_SUCCESS, ACT_ADD } from './types';

const actsFetch = () => {
    return (dispatch) => {
        axios.get(getHostUri() + '/acts')
            .then(response => {
                dispatch(actsFetchSuccess(response.data.map(act => {
                    act.date = moment(act.date).toDate();
                    return act;
                })));
            })
            .catch(() => {
                dispatch(actsFetchFail());
            });
    };
};

const actsFetchSuccess = (acts) => {
    return {
        type: ACTS_FETCH_SUCCESS,
        payload: { acts }
    };
};

const actsFetchFail = () => {
    return {
        type: ACTS_FETCH_FAIL
    };
};

const createAct = (act) => {
    return {
        type: ACT_ADD,
        payload: act
    }
};

export { actsFetch, actsFetchSuccess, actsFetchFail, createAct };

