import { orderBy } from 'lodash';
import { ACTS_FETCH_FAIL, ACTS_FETCH_SUCCESS, ACT_ADD } from './types';

const acts = (state = [], action) => {
    switch (action.type) {
        case ACTS_FETCH_SUCCESS:
            return action.payload.acts;
        case ACTS_FETCH_FAIL:
            return [ ...state ];
        case ACT_ADD:
            return orderBy([...state, action.payload], 'date', 'desc');
        default:
            return state;
    }
}

export { acts };
