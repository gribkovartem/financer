import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import React from 'react';
import { Link } from 'react-router-dom';

export const LeftMenuItem = (props) => {
    const { title, handleClick, link, iconComponent } = props;

    return(
        <ListItem
            button
            onClick={() => handleClick()}
            component={props => <Link to={{pathname: link}} {...props} />}>
            <ListItemIcon>
                {iconComponent}
            </ListItemIcon>
            <ListItemText primary={title} />
        </ListItem>
    );
}