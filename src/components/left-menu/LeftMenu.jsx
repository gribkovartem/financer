import Drawer from '@material-ui/core/Drawer';
import List from '@material-ui/core/List';
import { withStyles } from '@material-ui/core/styles';
import AnnouncementIcon from '@material-ui/icons/Announcement';
import AttachMoneyIcon from '@material-ui/icons/AttachMoney';
import CalendarTodayIcon from '@material-ui/icons/CalendarToday';
import React, { Component } from 'react';
import { LeftMenuItem } from './LeftMenuItem';

const styles = {
    list: {
        width: 250
    }
};

class LeftMenuRoot extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        const { classes, toggleMenu } = this.props;

        return(
            <Drawer open={this.props.opened} onClose={() => toggleMenu(false)}>
                <div className={classes.list}>
                    <List>
                        <LeftMenuItem
                            title="Подсказки"
                            link="/hints"
                            iconComponent={<AnnouncementIcon />}
                            handleClick={() => toggleMenu(false)} />
                        <LeftMenuItem
                            title="Доходы/расходы"
                            link="/actions"
                            iconComponent={<AttachMoneyIcon />}
                            handleClick={() => toggleMenu(false)} />
                        <LeftMenuItem
                            title="Календарь"
                            link="/calendar"
                            iconComponent={<CalendarTodayIcon />}
                            handleClick={() => toggleMenu(false)} />
                    </List>
                </div>
            </Drawer>
        );
    }
}

export const LeftMenu = withStyles(styles)(LeftMenuRoot);