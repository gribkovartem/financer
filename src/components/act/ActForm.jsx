import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';
import { withStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import * as moment from 'moment';
import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { categories } from '../category';
import { typesNames } from './act-types';

const styles = {
    form: {
        maxWidth: '350px',
        display: 'flex',
        flexDirection: 'column'
    },
    formControl: {
        // marginBottom: '15px'
    },
    controls: {
        maxWidth: '350px'
    }
};

class ActFormRoot extends Component {
    constructor(props) {
        super(props);
        this.state = {
            act: {
                type: typesNames.expense,
                amount: 0,
                date: new Date(),
                category: 0
            }
        };
    }

    changeType = (event) => {
        this.setState({act: {...this.state.act, type: event.target.value}});
    }

    changeAmount = (event) => {
        let amount = event.target.value;
        if (this.state.type === typesNames.expense) {
            amount = amount * -1;
        }
        this.setState({act: {...this.state.act, amount: amount}});
    }

    changeDate = (event) => {
        this.setState({act: {...this.state.act, date: moment(event.target.value).toDate()}});
    }

    changeCategory = (event) => {
        this.setState({act: {...this.state.act, category: event.target.value}});
    }

    render() {
        const { act } = this.state;
        const { classes, isShown, closeForm, createAct } = this.props;
        const preparedDate = moment(act.date).format('YYYY-MM-DDTH:mm');

        return(
            <div>
                <Dialog
                    open={isShown}
                    onClose={closeForm}
                    aria-labelledby="alert-dialog-title"
                    aria-describedby="alert-dialog-description"
                >
                    <DialogContent className={classes.form}>
                        <FormControl className={classes.formControl}>
                            <InputLabel htmlFor="type">Тип</InputLabel>
                            <Select
                                value={act.type}
                                onChange={this.changeType}
                                inputProps={{
                                    id: 'type'
                                }}
                            >
                                <MenuItem value={0}>Расход</MenuItem>
                                <MenuItem value={1}>Доход</MenuItem>
                            </Select>
                        </FormControl>
                        <FormControl className={classes.formControl}>
                            <TextField
                                label="Сумма"
                                margin="normal"
                                type="number"
                                defaultValue={act.amount}
                                onChange={this.changeAmount}
                            />
                        </FormControl>
                        <FormControl className={classes.formControl}>
                            <TextField
                                label="Дата"
                                margin="normal"
                                type="datetime-local"
                                defaultValue={preparedDate}
                                onChange={this.changeDate}
                            />
                        </FormControl>
                        <FormControl className={classes.formControl}>
                            <InputLabel htmlFor="category">Категория</InputLabel>
                            <Select
                                value={act.category}
                                onChange={this.changeCategory}
                                inputProps={{
                                    id: 'category',
                                    margin: 'normal'
                                }}
                            >
                                {categories.map(category => {
                                    return <MenuItem key={category.id} value={category.id}>{category.name}</MenuItem>
                                })}
                            </Select>
                        </FormControl>
                    </DialogContent>
                    <DialogActions className={classes.controls}>
                        <Button onClick={() => closeForm()} color="primary">Отменить</Button>
                        <Button onClick={() => { createAct(act); closeForm(); }} color="primary" autoFocus>Сохранить</Button>
                    </DialogActions>
                </Dialog>
            </div>
        );
    }
}

ActFormRoot.propTypes = {
    classes: PropTypes.object.isRequired,
    isShown: PropTypes.bool.isRequired,
    closeForm: PropTypes.func.isRequired
};

export const ActForm = withStyles(styles)(ActFormRoot);