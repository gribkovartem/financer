const typesNames = {
    expense: 0,
    income: 1
};
const actionTypes = {
    [typesNames.expense]: 'Расход',
    [typesNames.income]: 'Доход'
};

export { typesNames, actionTypes };