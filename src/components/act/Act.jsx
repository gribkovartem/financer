import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import { typesNames } from './act-types';

const styles = {
    card: {
        maxWidth: '512px',
        width: '100%',
        display: 'flex',
        marginBottom: '10px',
        '&:last-of-type': {
            marginBotton: 0
        }
    },
    cardContent: {
        display: 'flex',
        flex: 1
    },
    cardMeta: {
        maxWidth: '100px',
        width: '100%'
    },
    amount: {
        flex: 1,
    },
    sum: {
        display: 'inline-block',
        marginRight: '10px',
    },
    currencySymbol: {
        display: 'inline-block',
    },
    type: {
        width: '20px',
        height: 'auto'
    },
    typeRed: {
        color: '#f44336'
    },
    typeGreen: {
        color: '#4CAF50'
    }
};

const ActRoot = ({
    classes,
    type,
    amount,
    date,
    category
}) => {
    const color = type === typesNames.expense ? classes.typeRed : classes.typeGreen;
    const amountClasses = `${classes.sum} ${color}`;

    return(
        <Card className={classes.card}>
            <CardContent className={classes.cardContent}>
                <div className={classes.amount}>
                    <Typography variant="display2" className={amountClasses}>{amount}</Typography>
                    <Typography variant="display1" className={classes.currencySymbol}>₽</Typography>
                </div>
                <div className={classes.cardMeta}>
                    <Typography variant="body2">{date}</Typography>
                    <Typography variant="caption">{category}</Typography>
                </div>
            </CardContent>
        </Card>
    );
};

export const Act = withStyles(styles)(ActRoot);