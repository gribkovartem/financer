import Button from '@material-ui/core/Button';
import { withStyles } from '@material-ui/core/styles';
import AddIcon from '@material-ui/icons/Add';
import * as moment from 'moment';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Act, ActForm } from '../../components/act';
import { categories } from '../../components/category';
import { actsFetch, createAct } from '../../store/act/actions';

const styles = (theme) => ({
    actions: {
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center'
    },
    addButton: {
        margin: theme.spacing.unit,
        position: 'fixed',
        right: 0,
        bottom: 0
    }
});

class ActListRoot extends Component {
    constructor(props) {
        super(props);
        this.state = {
            formShown: false
        };
    }

    componentDidMount() {
        this.props.actsFetch();
    }

    render() {
        const { classes, createAct, acts } = this.props;
        const { formShown } = this.state;
        const actions = acts ? acts.map(action => {
            const category = categories.find(category => category.id === action.category);

            return <Act
                key={action.id ? action.id : Math.abs(action.amount + action.type)}
                type={action.type}
                amount={action.amount}
                date={moment(action.date).format('DD.MM H:mm')}
                category={category.name} />
        }) : null;

        return (
            <div className={classes.actions}>
                {actions}
                <Button
                    variant="fab"
                    color="secondary"
                    aria-label="Add"
                    className={classes.addButton}
                    onClick={() => {
                        this.setState({ formShown: true });
                    }}>
                    <AddIcon />
                </Button>
                <ActForm createAct={createAct} isShown={formShown} closeForm={() => { this.setState({ formShown: false }) }} />
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        acts: state.acts
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        actsFetch: bindActionCreators(actsFetch, dispatch),
        createAct: bindActionCreators(createAct, dispatch)
    };
};

export const ActList = connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(ActListRoot));