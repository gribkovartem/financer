const host = 'http://localhost';
const port = '9000';

const getHostUri = () => {
    return `${host}:${port}`;
}

export { host, port, getHostUri };