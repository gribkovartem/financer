import { createMuiTheme, MuiThemeProvider, withStyles } from '@material-ui/core/styles';
import React, { Component } from 'react';
import { Provider } from 'react-redux';
import { BrowserRouter as Router, Redirect, Route, Switch } from 'react-router-dom';
import { applyMiddleware, combineReducers, createStore } from 'redux';
import thunk from 'redux-thunk';
import { Header } from './components/header';
import { HintsList } from './components/hint';
import { LeftMenu } from './components/left-menu';
import { ActList } from './containers/act';
import { acts } from './store/act/reducers';

const theme = createMuiTheme({
    palette: {
        primary: {
            main: '#546e7a',
            light: '#819ca9',
            dark: '#29434e',
            contrastText: '#ffffff'
        },
        secondary: {
            main: '#f06292',
            light: '#ff94c2',
            dark: '#ba2d65',
            contrastText: '#000000'
        }
    }
});

const styles = {
    root: {
        display: 'flex',
        flexDirection: 'column',
        height: '100%'
    },
    content: {
        flexGrow: 1,
        padding: '10px'
    }
}

const store = createStore(
    combineReducers({ acts }),
    applyMiddleware(thunk)
);

class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            menuOpened: false
        };
    }

    toggleMenu = (opened) => () => {
        this.setState({menuOpened: opened});
    }

    render() {
        const { classes } = this.props;
        const { menuOpened } = this.state;

        return (
            <Router>
                <MuiThemeProvider theme={theme}>
                    <Provider store={store}>
                        <div className={classes.root}>
                            <LeftMenu opened={this.state.menuOpened} toggleMenu={this.toggleMenu()} />
                            <Header toggleMenu={this.toggleMenu(!menuOpened)} />
                            <div className={classes.content}>
                                <Switch>
                                    <Route path="/" exact>
                                        <Redirect to="/hints" />
                                    </Route>
                                    <Route path="/hints" component={HintsList} />
                                    <Route path="/actions" component={ActList} />
                                </Switch>
                            </div>
                        </div>
                    </Provider>
                </MuiThemeProvider>
            </Router>
        );
    }
}

export default withStyles(styles)(App);